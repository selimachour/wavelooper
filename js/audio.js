/* jshint esversion: 6 */

var path = require('path');


const L = {

    // default values
    minPlaybackRate: 0.3,
    maxPlaybackRate: 1.7,

    // state
    isPlaying: false,
    songLoaded: false,
    songLoading: false,

    cursorPosition: 0, // global variable for detemining current song position in samples
    autoScroll: false, // scroll while playing

    // actual song
    songSource: null,
    songBuffer: null,

    loopStart: 0,
    loopEnd: 0,
    playbackRate: 1,
    
    // fake song played along the main one to determine position (sad but true)
    currentPosition: null,
    positionSource: null,
    positionBuffer: null,

    // drawing options
    peakSamples: 200,
    needsRefresh: false,
    sampleWidth: 0.01,
    sampleOffset: 0,

    //

    startLoopNow: () => { L.loopStart = L.x2sec(L.spl2x(L.currentPosition)); updateSourceOptions(); },
    endLoopNow:   () => { L.loopEnd   = L.x2sec(L.spl2x(L.currentPosition)); updateSourceOptions(); },

    // utility functions
    x2spl: (x) => (x / L.sampleWidth) + L.sampleOffset,
    spl2x: (s) => (s - L.sampleOffset) * L.sampleWidth,

    x2sec: (x) => L.x2spl(x) / audioContext.sampleRate,
    sec2x: (s) => L.spl2x(s * audioContext.sampleRate),
    sec2w: (s) => s * audioContext.sampleRate * L.sampleWidth,
    

    x2peak: (x) => ((x / L.sampleWidth) + L.sampleOffset) / L.peakSamples,
    peak2x: (s) => ((s * L.peakSamples) - L.sampleOffset) * L.sampleWidth,

    spl2y: (spl) => map(spl, -1.1, 1.1, canvas.height, 0)
};


// graphics
let canvas;  // reference to <canvas>
let ctx;     // reference to <cancas>'s context
var peak = null; // array of peaks for faster drawing;


// Audio Context
const audioContext = new AudioContext();
const gainNode = audioContext.createGain();
gainNode.gain.value = 0.5;
gainNode.connect(audioContext.destination);


// fx processor for reading from position stream
var positionProcessor = audioContext.createScriptProcessor(1024);
positionProcessor.connect(audioContext.destination);
positionProcessor.onaudioprocess = function (data) {
    var inputBuffer = data.inputBuffer;
    var inputData = inputBuffer.getChannelData(0);
    L.currentPosition = L.positionBuffer ? map(inputData[0], -1, 1, 0, L.positionBuffer.length) : 0;
};


function browseFiles() {
    const input = document.getElementById('fileDialog')
    input.addEventListener("change", function (evt) {
        stop();
        load(this.value);
        this.value = ""; // otherwise windows complains
    }, false);
    input.click(); 
}

function quit() {
    nw.App.quit();
}

function load(file, cb) {
    L.songLoaded = false;
    L.songLoading = true;
    L.needsRefresh = true;
    var request = new XMLHttpRequest();
    request.open('GET', 'file://' + file, true);
    request.responseType = 'arraybuffer';
    request.onload = function () {
        audioContext.decodeAudioData(request.response, function (buffer) {
            document.getElementById('songName').innerText = path.basename(file);
            L.songBuffer = buffer;
            L.songLoaded = true;
            L.songLoading = false;
            buildPeaks();
            L.needsRefresh = true;
            buildPosition();
            document.getElementById('playbackRate').value = 1;
            document.getElementById('pbr').innerText = 1;

            if (cb) cb();
        }, (e) => { alert(e); });
    };
    request.send();
}

function play() {
    if (!L.songLoaded || L.isPlaying)
        return;
    
    L.songSource = audioContext.createBufferSource();
    L.songSource.buffer = L.songBuffer;
    L.songSource.loop = true;
    
    L.positionSource = audioContext.createBufferSource();
    L.positionSource.buffer = L.positionBuffer;
    L.positionSource.loop = true;
    
    updateSourceOptions();
    
    L.songSource.connect(gainNode);
    L.positionSource.connect(positionProcessor);
    
    L.songSource.start(0, L.cursorPosition);
    L.positionSource.start(0, L.cursorPosition);
    
    L.isPlaying = true;
    document.body.classList.add('playing');
    L.needsRefresh = true;
}

function stop() {
    if (L.isPlaying) {
        L.songSource.stop();
        L.positionSource.stop();
        L.currentPosition = L.loopStart;
    }
    L.isPlaying = false;
    document.body.classList.remove('playing');
}


function buildPosition() {
    console.time('Building Position File');

    // build buffer the same length of main source
    L.positionBuffer = audioContext.createBuffer(1, L.songBuffer.length, audioContext.sampleRate);

    var buf = L.positionBuffer.getChannelData(0);
    for (var i = 0; i < L.positionBuffer.length; i++) {
        buf[i] = map(i, 0, L.positionBuffer.length, -1, 1);
    }

    L.positionSource = audioContext.createBufferSource();
    L.positionSource.buffer = L.positionBuffer;
    console.timeEnd('Building Position File');
}


function buildPeaks() {
    console.time('Building Peaks');

    const nbSamples = L.songBuffer.length;
    const samples = L.songBuffer.getChannelData(0);
    
    peak = [];
    let min = 0, max = 0;

    for (let i = 0; i < nbSamples; i++) {
        spl = samples[i];
        min = Math.min(min, spl);
        max = Math.max(max, spl);

        if ( (i-1) % L.peakSamples === 0 ) {
            peak.push([min, max]);
            min = max = 0;
        }
    }

    console.timeEnd('Building Peaks');
}

/******************************************  GUI INTERACTION *********/

function updatePlottingOptions() {
}

function updateSourceOptions() {
    if (L.songSource) {
        L.songSource.loopStart = L.positionSource.loopStart = L.loopStart;
        L.songSource.loopEnd   = L.positionSource.loopEnd   = L.loopEnd;
        L.songSource.playbackRate.value = L.positionSource.playbackRate.value = L.playbackRate;
    }
}


/******************************************  DRAWING  *************/

function init() {

    // init graphics
    canvas = document.getElementById('canvas');
    resizeCanvas();
    ctx = canvas.getContext('2d');

    // prevent focusing of buttons so that <space> does not trigger click
    document.querySelectorAll('#transport > *').forEach(function(e) {
        e.addEventListener('focus', function(e) { this.blur(); });
    });

    bindMouse();
    bindKeyboard();
    refreshCanvas();
}

function bindMouse() {
    canvas.addEventListener('mousewheel', function (e) {
        if (e.shiftKey) {
            // scroll with shift + mouse wheel
            let s = L.x2spl(-e.deltaY);
            L.sampleOffset = Math.max(s, 0); 
        } else {
            // zoom with mouse wheel
            let oldSplUnderMouse = L.x2spl(e.clientX);
            L.sampleWidth /= Math.exp(e.deltaY / 100);
            L.sampleOffset -= L.x2spl(e.clientX) - oldSplUnderMouse;
            if (L.sampleOffset < 0) L.sampleOffset = 0;
        }
        L.needsRefresh = true;
    });

    // click to set start of play
    canvas.addEventListener('click', function (e) {
        L.cursorPosition = L.x2sec(e.clientX);
        L.needsRefresh = true;
        if (L.isPlaying) {
            stop();
            play();
        }

    });

    // click to set start of play
    canvas.addEventListener('dblclick', function (e) {
        if (e.shiftKey) L.loopEnd = L.x2sec(e.clientX);
        else L.loopStart = L.x2sec(e.clientX);
        updateSourceOptions();
    });
}

function bindKeyboard() {
    window.addEventListener('keydown', (e) => {
        console.log(e.which);
        switch (e.which) {
            case 32: // <space> for play/stop
                if (L.songLoaded) {
                    L.isPlaying ? stop() : play();
                }
                if (!L.songLoaded && !L.songLoading) {
                    browseFiles();
                }
                break;
            case 38:
            case 40:
                if (e.shiftKey) { // <shift>+<UP/DOWN> for speed
                    let rate = L.songSource.playbackRate.value;
                    if (38 === e.which) rate += 0.02; else rate -= 0.02;
                    rate = Math.min(L.maxPlaybackRate, Math.max(L.minPlaybackRate, rate));
                    rate = Math.round(rate * 1000) / 1000;
                    L.songSource.playbackRate.value = rate;
                    L.positionSource.playbackRate.value = rate;

                    document.getElementById('playbackRate').value = rate;
                    document.getElementById('pbr').innerText = rate;

                } else { // <UP/DOWN> for volume
                    let vol = gainNode.gain.value;
                    if (38 === e.which) vol += 0.1; else vol -= 0.1;
                    if (vol > 1) vol = 1;
                    if (vol < 0) vol = 0;
                    gainNode.gain.value = vol;
                }
                break;
            case 72: // <H> for help
                document.getElementById('help').classList.toggle('hidden');
                break;
        }
    });
}

function refreshCanvas() {
    if (L.songLoading) {
        ctx.font = "30px Arial";
        ctx.fillText("Loading ...", 10, 50);
    } else if (L.songLoaded && L.needsRefresh) {
        console.time('Drawing wave');

        // resize canvas to fit app
        canvas.width = window.innerWidth - 8;
        canvas.height = window.innerHeight - 85;

        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        // zero line
        ctx.strokeStyle = '#444';
        ctx.beginPath();
        ctx.moveTo(0, L.spl2y(0));
        ctx.lineTo(canvas.width, L.spl2y(0));
        ctx.stroke();


        // draw wave (from peaks or wave ?)
        let spl = 0;
        ctx.strokeStyle = '#393';
        if (L.sampleWidth < 0.01) {
            let startPeak = Math.floor(L.x2peak(0));
            let endPeak = L.x2peak(canvas.width);

            ctx.beginPath();
            for (let i = startPeak; i < endPeak; i++) {
                spl = peak[i];
                if ('undefined' === typeof spl) break;
                let x = L.peak2x(i);
                ctx.moveTo(x, L.spl2y(spl[0]));
                ctx.lineTo(x, L.spl2y(spl[1]));
            }
            ctx.stroke();
        } else {
            const samples = L.songBuffer.getChannelData(0);
            let startSpl = Math.floor(L.x2spl(0));
            let endSpl = Math.ceil(L.x2spl(canvas.width));

            ctx.beginPath();
            ctx.moveTo(0, L.spl2y(0));
            for (let i = startSpl; i <= endSpl; i++) {
                spl = samples[i];
                if ('undefined' === typeof spl) break;
                ctx.lineTo(L.spl2x(i), L.spl2y(spl));
            }
            ctx.stroke();

            ctx.fillStyle = 'lime';
            if (L.sampleWidth > 10) {
                for (let i = startSpl; i < endSpl; i++) {
                    spl = samples[i];
                    if ('undefined' === typeof spl) break;
                    ctx.fillRect(L.spl2x(i) - 3, L.spl2y(spl) - 3, 7, 7);
                }
            }
        }

        // PlayBackPosition
        if (L.isPlaying) {
            ctx.strokeStyle = '#ccc';
            ctx.beginPath();
            ctx.moveTo(L.spl2x(L.currentPosition), 0);
            ctx.lineTo(L.spl2x(L.currentPosition), canvas.height);
            ctx.stroke();
        }
        
        // Play Cursor
        if (!L.isPlaying) {
            ctx.strokeStyle = 'yellow';
            ctx.beginPath();
            ctx.moveTo(L.sec2x(L.cursorPosition), 0);
            ctx.lineTo(L.sec2x(L.cursorPosition), canvas.height);
            ctx.stroke();
        }

        // loop markers
        if (L.loopEnd) {
            ctx.fillStyle = 'yellow';
            ctx.fillRect(
                L.sec2x(L.loopStart), 0,
                L.sec2w(L.loopEnd - L.loopStart), 2);
        } 

        // scroll
        if (L.isPlaying && L.autoScroll) {
            L.sampleOffset = L.currentPosition ? L.currentPosition : 0;
        }

        console.timeEnd('Drawing wave');
    }

    // refresh if playing
    L.needsRefresh = L.isPlaying;
    win.window.requestAnimationFrame(refreshCanvas);
}


function resizeCanvas() {
    L.needsRefresh = true;
}


/******************************************** SETTINGS */

function saveLastOpened(songPath) {
    var file = 'settings/lastOpened.json';
    var filePath = path.join(nw.App.dataPath, file);
    fs.writeFile(filePath, songPath, function (err) {
        if (err) {
            console.info("Error while saving last opened.");
            console.warn(err.message);
            return;
        } else if (callback) {
            callback();
        }
    });

}


/******************************************** TOOLS */


const map = (value, istart, istop, ostart, ostop) =>
    ostart + (ostop - ostart) * ((value - istart) / (istop - istart));



/**
 * TODO
 * 
 * 
 * onended !
 * save peak
 * more mouse actions
 * interface
 * more keyboard actions
 * don't create a new position buffer
 * read with fs
 * 
 */