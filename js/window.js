/* jshint esversion: 6 */

const win = nw.Window.get();

const MainMenu = new nw.Menu({ type: 'menubar' });
const FileMenu = new nw.Menu();
FileMenu.append(new nw.MenuItem({ label: 'Item A', click: browseFiles }));
FileMenu.append(new nw.MenuItem({ label: 'Item B' }));
MainMenu.append(new nw.MenuItem({
    label: 'File',
    submenu: FileMenu 
}));
//win.menu = MainMenu;


// win.showDevTools();

// autoresize canvas
win.window.addEventListener('resize', resizeCanvas);


/* Saving settings example 
var fs = require('fs');
var path = require('path');

function saveSettings(settings, callback) {
    var file = 'my-settings-file.json';
    var filePath = path.join(nw.App.dataPath, file);
    path.
    fs.writeFile(filePath, settings, function (err) {
        if (err) {
            console.info("There was an error attempting to save your data.");
            console.warn(err.message);
            return;
        } else if (callback) {
            callback();
        }
    });
}

var mySettings = {
    "language": "en",
    "theme": "dark"
};

saveSettings(mySettings, function () {
    console.log('Settings saved');
});
console.log(nw.App.dataPath);
*/